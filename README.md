**PWS: The Proxy Wave Server**

The Proxy Wave Server looks like a TriNet waveserver at the front where clients connect. In the back, it speaks the appropriate protocol to talk to various other wave servers such as Earthworm wave_serverV, Nanometrics server, NCEDC sws.

Optionally, pws can be compiled to get waveforms using the FDSN dataselect web services. For specification, see https://www.fdsn.org/webservices/fdsnws-dataselect-1.1.pdf. 

Be sure to get the latest qlib2 and related tools from UC Berkeley http://www.ncedc.org/ftp/pub/quanterra/ucb/

*--Pete Lombard, UCB BSL, August 2011*


**Build Instructions**

1.  Build the libraries.  [Update this later]
2.  Clean up old object files and executables.

    `make clean`

3.  Build the project.

     `make all`
