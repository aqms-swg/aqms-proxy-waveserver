/** @file
 * @ingroup group_proxy-waveserver
 * @brief Earthworm Proxy Wave Client class
 */
/***********************************************************

File Name :
        PWClientEW.h

Original Author:
        Pete Lombard

Description:
	Earthworm Proxy Wave Client class.


Creation Date:
        August 19, 2004


Modification History:
	January 30, 2008 - Changed to use leap second enabled lib/tntime 
	classes

	May 2, 2008 - Removed mapping of requested channel to waveserver 
		channel: no longer needed.

Usage Notes:


**********************************************************/

#ifndef pw_client_ew_H
#define pw_client_ew_H

#include <list>
#include <map>
#include "TimeWindow.h"
#include "Counter.h"
#include "PWClientBase.h"

#define PC_EWDBSTRING "EW.V5"

// Heirarchy of waveservers
// first, a list of servers typically serving mutually exclusive SCNLs
typedef std::list< WServer* > WServerList;

// each list of servers servers about the same SNCLs; all have same priority
typedef std::list< WServerList > WServerSet;

// Map of priority to server sets
typedef std::map<int, WServerSet, std::less<int> > WSSPrioList;

// Map of server group string (from DB) to server groups
typedef std::map<std::string, WSSPrioList, 
		 std::less<std::string> > WServerGroupMap;

// Group the iterators together for a given WServer group
typedef struct
{
    WSSPrioList::iterator wsmi0;
    WServerSet::iterator  wsmi1;
    WServerList::iterator wsmi2;
} WSMI;

// and map from server group string to iterator cache
typedef std::map<std::string, WSMI, std::less<std::string> > WSmiMap;

// Store pointers to the WServers and their priority for each channel
typedef std::multimap<int, WServer*, std::less<int> > WServerPrioList;

typedef std::map<Channel, WServerPrioList, std::less<Channel> > ChanServerMap;

class PWClientEW : public PWClient
{
 private:
    WServerGroupMap servers;
    WSmiMap wsmi;
    ChanServerMap chanServerMap;
    Counter reqid;

    int _getNextMenu( WSMI *itsp, WSSPrioList *wgp, WServerPrioList &sl, 
		      Channel &reqChan );
    void _parseMenu(char *buffer, Channel &reqChan, bool &chanFound, 
		    WServerList::iterator &wsi, int basePrio);
    int _getDataFromServer(WServer *ws, Channel &chan, TimeWindow &win,
			   DataArrayList &dc);
    int _tracebufToDAL(char *buffer, int bufflen, DataArrayList &dc, 
		       Channel &chan);
    

 public:
    PWClientEW(char *config);
    ~PWClientEW();
    int LoadConfig();
    int GetData(std::string WSGroup, Channel &reqchan, TimeWindow win, 
		DataSegmentList &dl);
  
};


#endif
