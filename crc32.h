/** @file
* @ingroup group_proxy-waveserver
* @brief Table-driven implementation of the standard CRC-32 algorithm which is used for zip utilities
*/
/*
 * crc32.h  CRC used by Nanometrcis
 */

#include <stdint.h>

#ifndef _CRC32_H
#define _CRC32_H

void crc32_init( void );
void crc32_update( unsigned char *blk_adr, uint32_t blk_len );
uint32_t crc32_value( void );

#endif 
