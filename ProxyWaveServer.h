/** @file
 * @ingroup group_proxy-waveserver
 * @brief translates between the AQMS-specific protocol and several other protocols, including the Earthworm protocol
 */
/***********************************************************

File Name :
        ProxyWaveServer.h

Original Author:
        Pete Lombard

Description:


Creation Date:
        August 19, 2004


Modification History:
	January 30, 2008 - Changed to use leap second enabled lib/tntime 
	classes


Usage Notes:


**********************************************************/

#ifndef proxy_wave_server_H
#define proxy_wave_server_H

// Various include files
#include <list>
#include <map>
#include <string>
#include "TimeStamp.h"
#include "Duration.h"
#include "Channel.h"
#include "TimeWindow.h"
#include "DataSegment.h"
#include "WaveServer.h"
#include "DatabasePWS.h"
#include "PWClientEW.h"
#include "PWClientNano.h"
#include "pwscb.h"


// Type definition of an Address list
typedef std::list<Client, std::allocator<Client> > AddressList;

// Type definition of a PWClient map
typedef std::map< std::string, PWClient*, std::less<std::string>,
		  std::allocator<std::pair<const std::string, PWClient*> > > PWClientMap;


class ProxyWaveServer : public WaveServer
{
 private:
    PWClientMap pwcmap;
    char ewconf[FILENAME_MAX];
    char nanoconf[FILENAME_MAX];
    char swsconf[FILENAME_MAX];
#ifdef IRIS_WS
    char ws_conf[FILENAME_MAX];
    char pws_version[MAXSTR];
#endif    
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char appName[MAXSTR];
    DatabasePWS *db;
    bool db_initialized;
    bool no_rdns;

    int _InitDB();

 public:
    ProxyWaveServer(unsigned int portnum, char *logfilename, int semkey,
		    char *ewfile, char *nanofile, char *swsfile, char *ws_file);
    ProxyWaveServer(pwscb);
    ~ProxyWaveServer();
    int _GetData(Channel &chan, TimeWindow win, DataSegmentList &dl);
    int _GetTimes(Channel chan, TimeWindowList &tl);
    int _GetSampleRate(Channel &chan);
    int _GetChannels(ChannelList &cl);
    int Init();
    
};


#endif

