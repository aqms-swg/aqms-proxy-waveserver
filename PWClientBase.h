/** @file
 * @ingroup group_proxy-waveserver
 * @brief Base class for the Proxy Wave Client objects
 */
/***********************************************************

File Name :
        PWClientBase.h

Original Author:
        Pete Lombard

Description:
	Base class for the Proxy Wave Client objects.
	The Client objects are clients of each of the different
	real wave servers to which PWS talks.

Creation Date:
        August 19, 2004


Modification History:
	January 30, 2008 - Changed to use leap second enabled lib/tntime 
	classes

	May 2, 2008 - Removed mapping of requested channel to waveserver 
		channel: no longer needed.

	June 6, 2008 - Made miniSEED compression mode configurable.

Usage Notes:


**********************************************************/

#ifndef pw_client_base_H
#define pw_client_base_H

#include <cmath>
#include <deque>
#include <string>
#include "RetCodes.h"
#include "Duration.h"
#include "Channel.h"
#include "TimeWindow.h"
#include "DataArray.h"
#include "DataSegment.h"
#include "Waveform.h"
#include "WaveServer.h"
#include "WServer.h"

typedef std::deque<std::string, std::allocator<std::string> > WordList;

typedef std::list<DataArray, std::allocator<DataArray> > DataArrayList;


class PWClient 
{
 private:
    int recSize;
    int compMode;

 protected:
    int valid;
    int connTimeout;
    int sendTimeout;
    int replyTimeout;
    char configfile[MAXSTR];

    double _dataCompleteness(TimeWindow win, DataArrayList &dc, 
			     double samplerate);
    void _processData(DataArrayList &dc, Channel &chan, DataSegmentList &dl);
    void _split (std::string& text, WordList& words);
    
 public:
    PWClient();
    PWClient(char *config);
    ~PWClient();
    
    virtual int LoadConfig() = 0;
    virtual int GetData(std::string WSGroup, Channel &reqchan,
			TimeWindow win, DataSegmentList &dl) = 0;

    // These aren't implemented currently, so the base class defines
    // them to return errors
    int GetSampleRate(Channel &chan);
    int GetTimes(Channel chan, TimeWindowList &tl);
    int GetChannels(ChannelList &cl);
    
    void setConnTimeout(int timeout);
    int getConnTimeout();
    void setSendTimeout(int timeout);
    int getSendTimeout();
    void setReplyTimeout(int timeout);
    int getReplyTimeout();

    int setRecSize(int recsize);
    int getRecSize();
    int setCompMode(int compMode);
    int getCompMode();
    
    int operator!();
};


#endif
