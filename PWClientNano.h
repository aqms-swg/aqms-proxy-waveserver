/** @file
 * @ingroup group_proxy-waveserver
 * @brief Nanometrics Proxy Wave Client class
 */
/***********************************************************

File Name :
        PWClientNano.h

Original Author:
        Pete Lombard

Description:
	Nanometrics Proxy Wave Client class.

Creation Date:
        November 8, 2004

Modification History:
	January 30, 2008 - Changed to use leap second enabled lib/tntime 
	classes

	May 2, 2008 - Removed mapping of requested channel to waveserver 
		channel: no longer needed.

	19 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef pw_client_nano_H
#define pw_client_nano_H

#include <list>
#include <map>
#include "TimeWindow.h"
#include "DataArray.h"
#include "Counter.h"
#include "NanoServer.h"
#include "PWClientBase.h"

extern "C" {
#include "nmx_pws_api.h"
}

#define PC_NANODBSTRING "NAQS-DAP.V1"

// Heirarchy of waveservers
// first, a list of servers typically serving mutually exclusive SCNLs
typedef std::list< NanoServer* > NServerList;

// each list of servers servers about the same SNCLs; all have same priority
typedef std::list< NServerList > NServerSet;

// Map of priority to server sets
typedef std::map<int, NServerSet, std::less<int> > NSSPrioList;

// Map of server group string (from DB) to server groups
typedef std::map<std::string, NSSPrioList, 
		 std::less<std::string> > NServerGroupMap;

// Group the iterators together for a given NServer group
typedef struct
{
    NSSPrioList::iterator wsmi0;
    NServerSet::iterator  wsmi1;
    NServerList::iterator wsmi2;
} NSMI;

// and map from server group string to iterator cache
typedef std::map<std::string, NSMI, std::less<std::string> > NSmiMap;

// Store pointers to the NanoServers and their priority for each channel
typedef std::multimap<int, NanoServer*, std::less<int> > NServerPrioList;

typedef std::map<Channel, NServerPrioList, std::less<Channel> > ChanNServerMap;

// Map for NMX channels and their keys
typedef std::map<Channel, int, std::less<Channel> > NMXChanMap;

// A channel map for each server
typedef std::map<WServer*, NMXChanMap, std::less<WServer*> > NMXServerMap;

class PWClientNano : public PWClient
{
 private:
    NServerGroupMap servers;
    NSmiMap wsmi;
    ChanNServerMap chanServers;
    NMXServerMap nmxServerMap;
    Counter reqid;

    int _getNextMenu( NSMI *itsp, NSSPrioList *wgp, NServerPrioList &sl, 
		      Channel &chan );
    void _parseMenu(NMX_CHANNEL_INFO *chanlist, int nChan, Channel &wsChan, 
		    bool &chanFound, NServerList::iterator &wsi, int basePrio);
    int _getDataFromServer(NanoServer *ws, Channel &chan, TimeWindow &win,
			   DataArrayList &dc);
    int _nmxtodc(char *buffer, NMXHDR &msghdr, DataArrayList &dc,
		 Channel &chan, double &nextWouldStart, int32_t &startval);
    

 public:
    PWClientNano(char *config);
    ~PWClientNano();
    int LoadConfig();
    int GetData(std::string NSGroup, Channel &reqchan, TimeWindow win, 
		DataSegmentList &dl);
  
};


#endif
