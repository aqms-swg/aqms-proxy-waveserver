/** @file
 * @ingroup group_proxy-waveserver
 * @brief Encapsulates the WaveServers with which the PWS is communicating
 */
/***********************************************************

File Name :
        WServer.h

Original Author:
        Pete Lombard

Description:
	This base class represents the waveservers with which the
	Proxy Wave Server is communicating

Creation Date:
        August 19, 2004


Modification History:
	May 2, 2008 - Removed mapping of requested channel to waveserver 
		channel: no longer needed.

Usage Notes:


**********************************************************/

#ifndef _wserver_h
#define _wserver_h

#include <iostream>
#include "TCPConnClientBuff.h"

using std::ostream;

const int WS_ERR_MAXRETRIES = -99;
const int WS_DEF_MAX_RETRIES = 10;
const int WS_DEF_BUFLEN = 8196;

// How to disconnect:
const int WS_MAYBE_DISCO = 1;
const int WS_DISCONNECT  = 2;
const int WS_ERROR_DISCO = 3;


class WServer
{
 protected:
    int valid;
    char host[MAXSTR];
    unsigned int port;
    bool closeAfterUse;
    int numberOfRetries;

 public:
    TCPConnClientBuff *conn;

    WServer();
    WServer(const WServer &s);
    WServer(char *addr, unsigned int ipport);
    ~WServer();
    void SetCloseAfterUse(bool cau);
    void SetMaxRetries(int max);
    bool TooManyRetries();
    bool CloseAfterUse();
    TCPConnClientBuff* Connect(Duration dur);
    int Disconnect(int mode);

    int operator!();
    friend ostream& operator<<(ostream &os, const WServer &ws);
    friend int operator<(const WServer &ws1, const WServer &ws2);
    friend int operator==(const WServer &ws1, const WServer &ws2);
};

#endif
