/** @file
 * @ingroup group_proxy-waveserver
 * @brief Client-side TCP connections with a buffer
 */
/***********************************************************

File Name :
        TCPConnClientBuff.h

Original Author:
        Pete Lombard

Description:
	Client-side TCP connections with a buffer
	Does NOT use the message-encoding scheme of TCPConn class
	It is debatable whether this really should be derived from TCPConn or
	should be a separate base class.

Creation Date:
        7 September 2004


Modification History:


Usage Notes:


**********************************************************/

#ifndef tcp_conn_client_buff_H
#define tcp_conn_client_buff_H

// Various include files
#include <sys/socket.h>
#include "TCPConn.h"
#include "Client.h"

class TCPConnClientBuff : public TCPConn
{
 private:
    char *buffer;
    int bufflen;
    int buffstart;
    int buffend;  // points to the byte AFTER that last valid byte
    struct sockaddr claddr;
    struct sockaddr svaddr;
    bool haveAddr;
    
    int _Establish(const Client &addr, Duration dur);
    int _GetMore(int &len);
    
 public:

    TCPConnClientBuff();
    TCPConnClientBuff(const char *svhost, unsigned int svport, int newbufflen,
		      Duration dur);
    ~TCPConnClientBuff();
    
    int SendBuff(char *buff, Duration dur);
    int SendBuff(char *buff, int len, Duration dur);
    int SetBuffLen(int len);
    int GetLine(char *buff, int &bufflen, Duration dur);
    int GetBuff(char *buff, int &bufflen, Duration dur);
    void FlushBuff();
};

#endif
    
