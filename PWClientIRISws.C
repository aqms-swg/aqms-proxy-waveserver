/** @file
 * @ingroup group_proxy-waveserver
 * @brief IRIS Proxy Wave Client class
 */
/***********************************************************

File Name :
        PWClientIRISws.C

Original Author:
        Pete Lombard

Description:
	IRIS Web Services client

Creation Date:
        May 02, 2011

Modification History:
        Jan 27, 2012 - Added configurable send and reply timeouts.

		  19 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:

**********************************************************/

#include <cstdlib>
#include <cmath>
#include "Configuration.h"
#include "RetCodes.h"
#include "Compat.h"
#include "WServer.h"
#include "PWClientIRISws.h"
#include <string.h>

extern "C" {
#include "qlib2.h"
#include "md5.h"
}

const int RESP_OK = 200;
const int RESP_ERROR = 550;
const int MD5_LEN = 33;

PWClientIRISws::PWClientIRISws(char *config) : PWClient(config)
{
    data.buflen = 2 * 4096;
    data.buffer = new char[data.buflen];
    data.blocksize = 0;
    data.endData = 0;
    data.needBlockSize = true;
    curl_global_init(CURL_GLOBAL_ALL);
    data.handle = curl_easy_init();
}

PWClientIRISws::~PWClientIRISws() 
{
    curl_easy_cleanup(data.handle);
}


int PWClientIRISws::LoadConfig()
{
    if (valid != TN_TRUE)
	return( TN_FAILURE );

    // Read the config file
    int curline;
    Configuration c(configfile);
    char tag[CONFIG_MAX_STRING_LEN];
    char value[CONFIG_MAX_STRING_LEN];
    char origline[CONFIG_MAX_STRING_LEN];

    // Access to the group map
    char wsgroup[MAXSTR];
    IWSServerGroupMap::iterator wgi;  // points to a priority map

    // Access to the priority map
    int currentPrio = -1;
    IWSSPrioList::iterator wspi;    // points to a server set

    // Access to the server set
    IWSServerSet::iterator wssi;     // points to a server list

    int retval;
    
    if (!c) {
	std::cout << "Error (PWClientIRISws::loadConfig): Unable to open configuration file: " 
		  << configfile
		  << std::endl;
	return(TN_FAILURE);
    };

    while (1) {
	retval = c.next(tag, value);
	c.getLineNum(curline);
	if(retval != TN_SUCCESS) {
	    break;
	} else {
	    if (strcmp(tag, "ConnectTimeout") == 0) {
		setConnTimeout(atoi(value));
	    }
	    else if (strcmp(tag, "WSGroup") == 0) {
		if (strlen(value) > MAXSTR) {
		    std::cout << "Error (PWClientIRISws::loadConfig): WSGroup name too long - " 
			      << value << std::endl;
		    std::cout << "Error (PWClientIRISws::loadConfig): Unable to process line " 
			      << curline << std::endl;
		    return(TN_FAILURE);
		} else {
		    strcpy(wsgroup, value);
		    IWSSPrioList wssList;
		    std::pair<IWSServerGroupMap::iterator, bool> wg_insretval = servers.insert(IWSServerGroupMap::value_type(wsgroup,
									      wssList));
		    if (wg_insretval.second == false) {
			std::cout << "Error (PWClientIRISws::Loadconfig): duplicate WSGroup "
				  << wsgroup << std::endl;
			return( TN_FAILURE );
		    }
		    wgi = wg_insretval.first;
		    currentPrio = -1;
		}
	    }
	    else if (strcmp(tag, "SetPriority") == 0) {
		retval = atoi(value);
		if (retval < 0) {
		    std::cout << "Error (PWClientIRISws::loadConfig): Invalid value for SetPriority - " 
			      << retval << std::endl;
		    std::cout << "Error (PWClientIRISws::loadConfig): Unable to process line " 
			      << curline << std::endl;
		    return(TN_FAILURE);
		} else {
		    // Start a new set of servers
		    currentPrio = retval;
		    // Is this priority level same as a previous one?
		    wspi = (*wgi).second.find(currentPrio);
		    if (wspi == (*wgi).second.end()) {
			// Start a new IWSServerSet for this priority
			IWSServerSet wss;
			std::pair<IWSSPrioList::iterator, bool> wsp_insretval = 
			    (*wgi).second.insert(IWSSPrioList::value_type(currentPrio, wss));
			wspi = wsp_insretval.first;
		    }
		    // Start a new IWSServerList, always
		    IWSServerList wsl;
		    wssi = (*wspi).second.insert((*wspi).second.end(), wsl);
		}
	    }
	    else if (strcmp(tag, "URLBase") == 0) {
		if (strlen(value) > MAXSTR) {
		    std::cout << "Error (PWClientIRISws::loadConfig): URLBase too long - " 
			      << value << std::endl;
		    std::cout << "Error (PWClientIRISws::loadConfig): Unable to process line " 
			      << curline << std::endl;
		    return(TN_FAILURE);
		} else {
		    std::string url = value;
		    (*wssi).insert((*wssi).end(), url);
		}
	    } else {
		std::cout << "Error (PWClientIRISws::loadConfig): Unrecognized command on line "
			  << curline << std::endl;
		c.getFullLine(origline);
		std::cout << "Line: " << origline << std::endl;
		return(TN_FAILURE);
	    }
	}
    }
    if (retval != TN_EOF) {
	std::cout << "Error (PWClientIRISws::loadConfig): Error occurred reading config file "
		  << configfile << ", line " << curline << std::endl;
	return(TN_FAILURE);
    }

    return( TN_SUCCESS );
}

int PWClientIRISws::GetData(std::string wsGroup, Channel &reqChan,
			TimeWindow win, DataSegmentList &dl)
{
    int retval;
    double qual = -1;
    double lastQual = -1;

    // Clean up from last run
    data.currentData.clear();
    data.lastData.clear();
    data.needBlockSize = true;
    data.endData = 0;
    dl = data.currentData;
  
    IWSServerGroupMap::iterator wgi = servers.find(wsGroup);
    if (wgi == servers.end()) {
	std::cout << "PWClientIRISws: wsGroup not found in server map: "
		  << wsGroup << std::endl;
	// This isn't really an error; since our client doesn't
	// know what channels we serve, it may ask for one we
	// don't handle.
	return( TN_NODATA );
    }
    IWSSPrioList *wssp = &( wgi->second );
    
    for ( IWSSPrioList::iterator wsmi0 = wssp->begin(); wsmi0 != wssp->end(); 
	  wsmi0++) {
	for (IWSServerSet::iterator wsmi1 = wsmi0->second.begin(); 
	     wsmi1 != wsmi0->second.end(); wsmi1++) {
	    for (IWSServerList::iterator wsmi2 = wsmi1->begin(); 
		 wsmi2 != wsmi1->end(); wsmi2++) {
		retval = _getDataFromServer(*wsmi2, reqChan, win);
		if (retval != TN_SUCCESS) continue;
		qual = _dlCompleteness(win, reqChan.samprate);
		if (1.0 == qual ) {
		    dl = data.currentData;
		    return( TN_SUCCESS );
		}
		else if (lastQual > 0 && qual > lastQual) {
		    data.lastData = data.currentData;
		    data.currentData.clear();
		    lastQual = qual;
		}
	    }
	}
    }
    
    // If we get here, we have not yet found complete data for the wsChan
    // Return what we have, if any

    if (lastQual > 0.0) {
	dl = data.lastData;
	return( TN_SUCCESS );
    }
    else if (qual > 0.0) {
	dl = data.currentData;
	return( TN_SUCCESS );
    }

    // DEBUG; presumably no errors have occurred, so TN_NODATA seems like the
    // appropriate return value.
    std::cout << "At end of PWClientIRISws::GetData; returning TN_NODATA" 
	      << std::endl;
    return( TN_NODATA );
}



// Get data for the Channel and TimeWindow from this server
// If there is a problem with the server, mark the server bad;
//  if no data received, return TN_FAILURE
// else if any data is returned, return TN_SUCCESS
int PWClientIRISws::_getDataFromServer(std::string &url, Channel &chan, 
				   TimeWindow &win)
{
    char request[MAXSTR];
    char startstring[24];
    char endstring[24];
    CURLcode status;
    char user_agent[81];
    
    // Set up the URL for IRIS web services.
    // See http://www.iris.edu/ws/ for more info.
    snprintf(startstring, 24, "%04d-%02d-%02dT%02d:%02d:%02d.%03d",
	     win.start.year(), win.start.month_of_year(), 
	     win.start.day_of_month(), win.start.hour_of_day(),
	     win.start.minute_of_hour(), win.start.second_of_minute(),
	     win.start.u_seconds() / 1000);
    snprintf(endstring, 24, "%04d-%02d-%02dT%02d:%02d:%02d.%03d",
	     win.end.year(), win.end.month_of_year(), 
	     win.end.day_of_month(), win.end.hour_of_day(),
	     win.end.minute_of_hour(), win.end.second_of_minute(),
	     win.end.u_seconds() / 1000);
    snprintf(request,MAXSTR, "%s/query?net=%s&sta=%s&loc=%s&cha=%s&start=%s&end=%s",
	     url.c_str(), chan.network, chan.station, 
	     (strcmp(chan.location, "  ") == 0) ? "--" : chan.location,
	     chan.channel, startstring, endstring);
    
    // Set up curl for our request
    curl_easy_setopt(data.handle, CURLOPT_URL, request);
    curl_easy_setopt(data.handle, CURLOPT_TIMEOUT, (int32_t)getConnTimeout());

    // Supply a callback function to handle data from curl
    curl_easy_setopt(data.handle, CURLOPT_WRITEFUNCTION, &collectData);
    curl_easy_setopt(data.handle, CURLOPT_WRITEDATA, (void*)&data);

    // Set up a user_agent string as recommended by B. Weertman at IRIS
    Client *cl = WaveServer::getClient();
    if (cl) {
	snprintf(user_agent, 81, "pws/%s SRC_HOST=%s", pws_version.c_str(), 
		 cl->host);
    } else {
	snprintf(user_agent, 81, "pws/%s SRC_HOST=unknown", pws_version.c_str());
    }
    curl_easy_setopt(data.handle, CURLOPT_USERAGENT, (void*)user_agent);
    
    // Make the transaction
    status = curl_easy_perform(data.handle);
    if (status != CURLE_OK) {
	std::cout << "Error (PWClientIRISws::_getDataFromServer): curl_easy_perform: " 
		  << curl_easy_strerror(status) << std::endl;
	// Should we abort? Not if we got some data.
    }
    
    if (data.needBlockSize) {
	int32_t respCode;
	status = curl_easy_getinfo(data.handle, CURLINFO_RESPONSE_CODE, &respCode);
	if (status != CURLE_OK) {
	    std::cout << "Error (PWClientIRISws::_getDataFromServer): error getting server response code" 
		      << std::endl;
	} else {
	    std::cout << "Error (PWClientIRISws::_getDataFromServer): server response code: "
		      << respCode << " for " << request << std::endl;
	}
	return(TN_FAILURE);
    }

    // Check for partial blocks left in the buffer
    if (data.endData > 0) {
	std::cout << "Warning (PWClientIRISws::_getDataFromServer): " 
		  << data.endData << " bytes left in buffer; block size is "
		  << data.blocksize << std::endl;
	// Should we abort? Not if we got some data.
    }

    chan.samprate = data.samprate;
    
    return(TN_SUCCESS);
}

// This is the callback function we provide to libcurl to handle
// our requested data as it is received. It gets called by C code.
size_t collectData( void *ptr, size_t size, size_t nmemb, void *userdata)
{
    DataStruct *mem = (DataStruct *)userdata;

    size_t dataReceived = size * nmemb;
    size_t dataUsed = 0;
    size_t tmpBlockSize = (mem->needBlockSize) ? 512 : mem->blocksize;

    while (dataUsed < dataReceived) {
	// Move data into our buffer, without overflowing it
	size_t toMove = dataReceived - dataUsed;
	size_t spaceAvail = mem->buflen - mem->endData;
	if (toMove > spaceAvail) toMove = spaceAvail;
	memcpy(&(mem->buffer[mem->endData]), (char*)ptr + dataUsed, toMove);
	mem->endData += toMove;
	dataUsed += toMove;
	
	if (mem->needBlockSize) {
	    // We need to determine the size of miniSEED blocks
	    DATA_HDR *hdr = decode_hdr_sdr((SDR_HDR *)mem->buffer, 
					   mem->blocksize);
	    if (hdr == NULL) {
		int32_t respCode = 0l;
		CURLcode status = curl_easy_getinfo(mem->handle, 
						    CURLINFO_RESPONSE_CODE,
						    &respCode);
		if (status != CURLE_OK) {
		    std::cout << "Error (PWClientIRISws collectData): error getting server response code" 
			      << std::endl;
		} else if (respCode != 0 && respCode != 200) {
		    std::cout << "Error (PWClientIRISws collectData): server response code: "
			      << respCode << std::endl;
		} else {
		    std::cout << "Error (PWClientIRISws collectData): unable to decode sdr header"
			      << std::endl;
		}
		return( 0 ); // indicate an error to libcurl
	    }
	    mem->samprate = dsamples_in_time2(hdr->sample_rate, 
					 hdr->sample_rate_mult, USECS_PER_SEC);
	    tmpBlockSize = mem->blocksize = hdr->blksize;
	    
	    free_data_hdr(hdr);
	    mem->needBlockSize = false;
	}
	
	// Process as many whole blocks of data as we can
	DataSegment ds;
	size_t dataSent = 0;
	for (size_t i = 0; i + mem->blocksize <= mem->endData; i += mem->blocksize) {
	    DATA_HDR *hdr = decode_hdr_sdr((SDR_HDR *)(mem->buffer+i), 
					   mem->blocksize);
	    if (hdr == NULL) {
		std::cout << "Error (PWClientIRISws collectData): can't decoder SDR header"
			  << std::endl;
		return(0); // signal an error to libcurl
	    }
	    
	    ds.len = mem->blocksize;
	    ds.numsamp = hdr->num_samples;
	    
	    // leapseconds: requested window is in true epoch
	    // Set dataSegment start time to true epoch for completeness test
	    ds.start = TimeStamp(TRUE_EPOCH_TIME, int_to_tepoch(hdr->begtime));
	    
	    memcpy(ds.data, mem->buffer + i, mem->blocksize);
	    mem->currentData.push_back(ds);
	    free_data_hdr(hdr);
	    dataSent += mem->blocksize;
	}
	if (dataSent > 0) {
	    if (dataSent < mem->endData) {
		// We didn't use all the data; move the remaining stuff up
		memcpy(mem->buffer, mem->buffer+dataSent, 
			mem->endData - dataSent);
	    }
	    mem->endData -= dataSent;
	}
    }
    
    return(dataReceived);
}

// Evaluate the completeness of data in buffer relative to the time window
// Return values: 0.0: no data
//                1.0: data fills window with no gaps
//          intermediate values indicate some amount of missing data
double PWClientIRISws::_dlCompleteness(TimeWindow win, double samplerate)
{
    double interval = 1.0 / samplerate;
    double completeness = 0.0;
    double gap;
    double winDur = win.end - win.start;
    
    // Make sure our first DataArray has data within the window
    while ( ! data.currentData.empty()) {
	if (data.currentData.begin()->start > win.end) {
	    // all data is too new
	    return( completeness );
	}
	TimeStamp end = data.currentData.begin()->start + 
	    Duration(data.currentData.begin()->numsamp * interval);
	if (end > win.start) {
	    // Some of data is inside the window
	    break;
	}
	// All of first DataSegment is before the window; delete it
	data.currentData.pop_front();
    }
    if (data.currentData.empty()) return( completeness );

    completeness = 1.0;
    
    // Check currentData, win start times: may be extra data or gap
    TimeStamp dataStart( data.currentData.begin()->start );
    gap =  win.start - dataStart;

    if (dataStart < win.start) {
	// Extra data; don't need it, but don't mess with MSEED blocks
    } else {
	// A gap between data start time and requested start time
	completeness -= gap / winDur;
    }

    DataSegmentList::iterator thisDS, nextDS;
    for (thisDS = data.currentData.begin(); thisDS != data.currentData.end(); thisDS++) {
	nextDS = thisDS;
	nextDS++;
	// compute the time the next segment would start based on the current
	// start time and sample count.
	TimeStamp nextWouldStart(thisDS->start + thisDS->numsamp * interval);
	if (nextDS == data.currentData.end()) {
	    // No more data; set the final gap
	    gap =  nextWouldStart - win.end;
	    
	    if (nextWouldStart < win.end) {
		// Data ends before request window ends, a real gap
		// We count this one twice in our completeness measure
		completeness -= 2.0 * gap / winDur;
	    }
	    // Round to nearest sample interval
	    return( round(completeness / interval) * interval );
	}
	
	// Is there a gap between this and next segments?
	gap = nextDS->start - nextWouldStart;
	if (gap > 0) {
	    completeness -= gap / winDur;
	}
    }
    
    // We shouldn't get here; tests inside the loop are supposed to return
    std::cout << "Logic error (PWClient::_dlCompleteness):"
	      << " passed loop without return" << std::endl;
    
    // Round to nearest sample interval
    return( round(completeness / interval) * interval );
}

int PWClientIRISws::SetPWSVersion(char *v)
{
    pws_version = v;
    return TN_SUCCESS;
}
