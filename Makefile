########################################################################
#
# Makefile     : Proxy Wave Server
#
# Author       : Pete Lombard
#
# Last Revised : 28 March 2022
#
########################################################################

include $(DEVROOT)/shared/makefiles/Make.includes

# uncomment the following four lines if you want the IRIS Web Service client
# included in pws. Be sure to adjust CURL_INCL and CURL_LIBS for your
# installation
CURL_INCL = -I/usr/include
CURL_LIBS = -L/usr/lib -lcurl
WS_OBJ    = PWClientIRISws.o
WS_MAC    = -DIRIS_WS

BIN	= pws
INCL	= $(RTSTDINCL) $(EWINCL) $(CURL_INCL)
LIBS	= $(RTSTDLIBS_NOCMS) -lew $(CURL_LIBS) -lgfortran -lpthread


########################################################################

BINOBJS= Main.o ProxyWaveServer.o WaveServer.o PWClientBase.o PWClientEW.o \
      TCPConnClientBuff.o WServer.o DatabasePWS.o PWClientNano.o \
      NanoServer.o PWClientSWS.o nmx_pws_api.o nmxp_packet.o crc32.o md5c.o

TESTOBJS = TestWaveClient.o

all:$(BIN)

pws:$(BINOBJS) $(WS_OBJ)
	$(CC) $(CFLAGS) $(BINOBJS) $(WS_OBJ) -o $@ $(LIBS)

testclient:$(TESTOBJS)
	$(CC) $(CFLAGS) $(TESTOBJS) -o $@ $(LIBS)

.C.o: 
	$(CC) $< -c $(CFLAGS) $(INCL) $(WS_MAC)

.c.o:
	$(cc) $< -c $(CFLAGS) $(INCL)

clean:
	-rm -f *.o core $(BIN)
	-rm -rf SunWS_cache

dependclean:
	makedepend
	-rm -f Makefile.bak

veryclean: clean dependclean
	-rm -f *~

depend:
	makedepend -- $(CFLAGS) $(INCL) -- *.C > /dev/null 2>&1

########################################################################
# DO NOT DELETE THIS LINE -- make depend depends on it.
