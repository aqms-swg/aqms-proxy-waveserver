/** @file
 * @ingroup group_proxy-waveserver
 * @brief Definitions and prototypes useful to a client of a Nanometrics server
 */
/*
 * nmx_api.h
 *
 * Based on Earthworm code by:
 * Lynn Dietz  22 January 1999
 *
 * Modified by Pete Lombard 12 November 2004, to remove earthworm-specific calls
 *
 * Definitions and prototypes useful for a client of
 * Nanometrics server software such as NaqsServer.
 * Written to specifications described in: 
 *
 *    "Nanometrics Software Reference Manual",
 *    "Naqs Online Data Streams" chapter (last revised 24 Nov 1998).
 *
 * Added more definitions, structures, functions needed for
 * clients of Nanometrics DataServer, as described in:
 *    "Nanometrics Data Access Protocol" (last revised 27 June 2001).
 *
 *    19 Dec 2016 - 64-bit system compliance updates performed
 */

#ifndef _NMX_API_H
#define _NMX_API_H

#include "crc32.h"

/**********************************************************************
 *                            BYTE ORDER                              *
 *    The Nanometrics server software may run on Intel platforms,     *
 *    but the byte order of the socket stream between NMX server      *
 *    software and its clients is in network byte order.              *
 *                        Swap accordingly!                           *
 **********************************************************************/

/* Define a NMX message
 * ---------------------
 * 
 * Each NMX message is composed of a fixed length header (12 bytes),
 * followed by variable length content.
 *
 *   Header:   4 byte int   signature = 0x7ABCDE0F
 *             4 byte int   message type
 *             4 byte int   message content length = N
 *
 *   Content:  N byte string with internal format dependent on
 *             message type.
 */

typedef struct _NMXHDR
{
    int signature;   /* set to magic value to verify the start of a header  */ 
    int msgtype;     /* type of message to follow (defines internal format) */
    int msglen;      /* length (bytes) of message content to follow         */
} NMXHDR;

#define NMXHDR_LEN         12
#define NMXHDR_SIGNATURE   0x7ABCDE0F

/* NMX Server message types
**************************/
#define NMXMSG_COMPRESSED_DATA       1  /* data, SOH, transparent serial */
#define NMXMSG_DECOMPRESSED_DATA     4  /* decompressed data */
#define NMXMSG_TRIGGER               5
#define NMXMSG_EVENT                 6
#define NMXMSG_CHANNEL_LIST        150
#define NMXMSG_ERROR               190
#define NMXMSG_TERMINATE           200
#define NMXMSG_CONNECT_RESPONSE    207
#define NMXMSG_READY               208
#define NMXMSG_PRECIS_LIST         253
#define NMXMSG_CHANNEL_HEADER      256
#define NMXMSG_DATA_SIZE           257
#define NMXMSG_NAQS_TRIGGER        259
#define NMXMSG_NAQS_EVENT          260

/* NMX Client message types
**************************/
#define NMXMSG_CONNECT             100
#define NMXMSG_REQUEST_PENDING     110
#define NMXMSG_ADD_TIMESERIES      120
#define NMXMSG_ADD_SOH             121
#define NMXMSG_ADD_TRIGGER         122
#define NMXMSG_ADD_EVENTS          123
#define NMXMSG_REMOVE_TIMESERIES   130
#define NMXMSG_REMOVE_SOH          131
#define NMXMSG_REMOVE_TRIGGER      132
#define NMXMSG_REMOVE_EVENTS       133
#define NMXMSG_PRECISLIST_REQUEST  203
#define NMXMSG_CANCEL_REQUEST      205
#define NMXMSG_CONNECT_REQUEST     206
#define NMXMSG_CHANNELLIST_REQUEST 209
#define NMXMSG_CHANNELINFO_REQUEST 226
#define NMXMSG_DATA_REQUEST        227
#define NMXMSG_DATASIZE_REQUEST    229
#define NMXMSG_TRIGGER_REQUEST     231
#define NMXMSG_EVENT_REQUEST       232

/* Other definitions 
*******************/
#define NMX_FAILURE                 -1   /* function return for failure */
#define NMX_SUCCESS                  0   /* function return for success */

#define NMX_SUBTYPE_TIMESERIES       1   /* time series data subtype */
#define NMX_SUBTYPE_SOH              2   /* state-of-health data subtype */
#define NMX_SUBTYPE_TRANSSERIAL      9   /* transparent serial subtype */

#define NAQS_REQUEST_TIMEOUT        30   /* once a connection is made to
                                          * NaqsServer, a client has this 
                                          * many seconds to send the first 
                                          * NMXMSG_ADD* msg, otherwise  
                                          * the connection times out */

#define DS_REQUEST_TIMEOUT          20   /* once a connection is made to
                                          * DataServer, a client has this 
                                          * many seconds to send the first 
                                          * NMXMSG_*REQUEST msg, otherwise  
                                          * the connection times out */


#define NMXP_BYTES_PER_BUNDLE        17

/* Packet Type (1 byte, bit 5 = 1 for retransmitted packet)
**********************************************************/
#define NMXP_COMPRESSDATA_PKT         1
#define NMXP_STATUS_PKT               2
#define NMXP_TRANSPARENTSERIAL_PKT    6
#define NMXP_FILLER_PKT               9

/* Definitions useful for decoding Compressed data bundles.
   /* The format of a timeseries data bundle is as follows:
   *   1 byte:   compression bits (2 bits for each data set)
   *   4 bytes:  Compressed data set 1 
   *   4 bytes:  Compressed data set 2 
   *   4 bytes:  Compressed data set 3 
   *   4 bytes:  Compressed data set 3    
   **********************************************************/
#define NMXP_DATASET_PER_BUNDLE      4
#define NMXP_MAX_SAMPLE_PER_BUNDLE  16
#define NMXP_NOCOMPRESS  0  /* data set holds 1 data value         */
#define NMXP_1BYTE_DIFF  1  /* data set holds 4 1-byte differences */
#define NMXP_2BYTE_DIFF  2  /* data set holds 2 2-byte differences */
#define NMXP_4BYTE_DIFF  3  /* data set holds 1 4-byte difference  */

/* Bundle types (1 byte) - the first byte of each bundle.
 * The exception is the compressed data bundle, where the first byte 
 * contains the compression bits for that bundle, so its value will vary.
 ************************************************************************/
#define NMXP_VCXOCAL_BUNDLE           7 
#define NMXP_NULL_BUNDLE              9
#define NMXP_MINMAX1_BUNDLE          10
#define NMXP_MINMAX2_BUNDLE          11
#define NMXP_INSTLOG_BUNDLE          12
#define NMXP_GPSLOCATION_BUNDLE      13
#define NMXP_GPSSTATUS_BUNDLE        15
#define NMXP_D1THRESHTRIG_BUNDLE     20
#define NMXP_D2THRESHTRIG_BUNDLE     21
#define NMXP_D1STALTATRIG_BUNDLE     22
#define NMXP_D2STALTATRIG_BUNDLE     23
#define NMXP_EVENT_BUNDLE            24
#define NMXP_FASTEXTSOH_BUNDLE       32   
#define NMXP_SLOWEXTSOH_BUNDLE       33   
#define NMXP_SLOWINTSOH_BUNDLE       34
#define NMXP_GPSTIMEQUAL_BUNDLE      39
#define NMXP_GPSSATINFO_BUNDLE       40
#define NMXP_SERIALPORTMAP_BUNDLE    41
#define NMXP_TELEMPKTREADERR_BUNDLE  42
#define NMXP_SERIALPORTERR_BUNDLE    43
#define NMXP_RXSLOTSTATE_BUNDLE      44
#define NMXP_TXSLOTERR_BUNDLE        45
#define NMXP_RXSLOTERR_BUNDLE        47
#define NMXP_LIBRAINSTSOH_BUNDLE     48
#define NMXP_LIBRAENVSOH_BUNDLE      49
#define NMXP_TRANSMITTER_BUNDLE      50
#define NMXP_RECEIVER_BUNDLE         51
#define NMXP_BURST_BUNDLE            52
#define NMXP_EPOCH_BUNDLE            53
#define NMXP_LIBRAGPSTIMEQUAL_BUNDLE 54
#define NMXP_LIBRASYSTIMEQUAL_BUNDLE 55
#define NMXP_LIBRAOPSTATE_BUNDLE     56
#define NMXP_SERIALDATABYTES_BUNDLE  57


/* Structure filled from a NMXMSG_CHANNEL_LIST message
******************************************************/
typedef struct _NMX_CHANNEL_INFO
{
    int   chankey;     /* equals ( (ID<<16) | (type<<8) | channel ) where      */
    /*   ID is the full instrument serial number            */
    /*   type is the data subtype (1=time series, 2=SOH)    */
    /*   channel is the data channel # (0 to 5)             */
    char  stachan[12]; /* null-terminated channel name string (e.g. STN01.BHZ) */
    short instrid;     /* derived from chankey */
    int   subtype;     /* derived from chankey */
    int   channel;     /* derived from chankey */
} NMX_CHANNEL_INFO;

/* Structure filled from a NMXMSG_PRECIS_LIST message
****************************************************/
typedef struct _NMX_PRECIS_INFO
{
    int   chankey;     /* equals ( (ID<<16) | (type<<8) | channel ) where      */
    /*   ID is the full instrument serial number            */
    /*   type is the data subtype (1=time series, 2=SOH)    */
    /*   channel is the data channel # (0 to 5)             */
    char  stachan[12]; /* null-terminated channel name string (e.g. STN01.BHZ) */
    short instrid;     /* derived from chankey */
    int   subtype;     /* derived from chankey */
    int   channel;     /* derived from chankey */
    int32_t  tstart;      /* start time of data available, sec since 1970/1/1     */
    int32_t  tend;        /* end time of data available, sec since 1970/1/1       */
} NMX_PRECIS_INFO;

/* Structure filled from a NMXMSG_COMPRESSED_DATA or 
   a NMXMSG_DECOMPRESSED_DATA message
***********************************************************/
typedef struct _NMX_DECOMP_DATA
{
    int    chankey;    /* same as NMX_CHANNEL_INFO structure            */
    double starttime;  /* time of 1st sample (seconds since 1 Jan 1970) */
    int    nsamp;      /* number of samples in this packet = N          */
    int    isamprate;  /* sample rate (samples per second)              */
    int    maxdatalen; /* maximum number of bytes in data               */
    int   *data;       /* N samples as 32-bit integers                  */ 
} NMX_DECOMP_DATA;

/* Structure filled from a NMXMSG_TERMINATE message
***************************************************/
#define NMX_NORMAL_SHUTDOWN     1   /* normal shutdown                */
#define NMX_ERROR_SHUTDOWN      2   /* shutdown due to error          */
#define NMX_TIMEOUT_SHUTDOWN    3   /* shutdown due to server timeout */

typedef struct _NMX_TERMINATE
{
    int    reason;     /* reason for termination: 1 = normal shutdown   */
    /*                         2 = error shutdown    */
    /*                         3 = timeout shutdown  */
    char  *note;       /* char string giving explanation of termination */
} NMX_TERMINATE;

/* Structure of info needed to build NMXMSG_CONNECT_REQUEST message
******************************************************************/
#define NMX_DS_LEN  12
typedef struct _NMX_CONNECT_REQUEST
{
    int32_t    DAPversion;       /* Data Access Protocol version           */
    char   user[NMX_DS_LEN];  /* DataServer user name (null terminated) */
    char   pswd[NMX_DS_LEN];  /* DataServer password  (null terminated) */
    int32_t   tconnect;          /* connection time returned by DataServer */
} NMX_CONNECT_REQUEST;


/* Compressed Data packet: header bundle
***************************************/
typedef struct _NMXPDATAHDR 
{
    char  pkttype;       /* 1 byte  packet type = 1 (bit 5=ReTx bit) */
    int32_t  sec;           /* 4 byte  whole seconds since 1970/01/01   */
    short subsec;        /* 2 byte  10,000ths of second              */
    short instrumentid;  /* 2 byte  instrumentid:  5 bit model type, */
    /*                       11 bit serial#     */
    int32_t  seqnum;        /* 4 byte  sequence number                  */
    char  isamprate;     /* 1 byte  5 bits for sample rate index     */
    char  chan;          /*         3 bits for channel number        */
    int   firstsample;   /* 3 byte  first sample value               */
} NMXPDATAHDR;

/* Compressed Data packet: data bundle
*************************************/
typedef struct _NMXPDATAVAL
{
    int  ndata;
    int32_t  data[NMXP_MAX_SAMPLE_PER_BUNDLE];
} NMXPDATAVAL;

/* API for building and reading NMX messages
*******************************************/
char *nmx_getlasterror() ;
void nmx_clearlasterror();
int  nmx_checklen( char **buf, int *buflen, int reqlen ); 
void nmx_unloadhdr( NMXHDR *nhd, char *chd ); 

/* These functions are used in Earthworm NMX clients: */
int nmx_bld_connect( char **buf, int *buflen, int *totlen ); 
int nmx_bld_connectrequest( char **buf, int *buflen, int *totlen,
                            NMX_CONNECT_REQUEST *conreq );
int nmx_bld_request_pending( char **buf, int *buflen, int *totlen ); 
int nmx_bld_add_timeseries( char **buf, int *buflen, int *totlen, int *keys,
                            int nkeys, int delay, int format, int sendbuffer ); 
int nmx_bld_add_soh( char **buf, int *buflen, int *totlen, int *keys,
		     int nkeys, int delay, int sendbuffer ); 
int nmx_bld_channellist_request( char **buf, int *buflen, int *totlen ); 
int nmx_bld_precislist_request( char **buf, int *buflen, int *totlen,
				int instrid, int dtype, int chan ); 
int nmx_bld_data_request(char **buf, int *buflen, int *totlen,
			 int chankey, int starttime, int endtime );
int nmx_bld_terminate( char **buf, int *buflen, int *totlen, 
                       NMX_TERMINATE *term ); 
int nmx_rd_decompress_data( NMXHDR *nhd, char *msg,  
                            NMX_DECOMP_DATA *naqsbuf ); 
int nmx_rd_channel_list( NMXHDR *nhd, char *msg,  
                         NMX_CHANNEL_INFO **naqschan, int *nchan );
int nmx_rd_precis_list( NMXHDR *nhd, char *msg,  
                        NMX_PRECIS_INFO **naqschan, int *nchan );
int nmx_rd_terminate( NMXHDR *nhd, char *msg, NMX_TERMINATE *term );
int nmx_rd_compressed_data( NMXHDR *nhd, char *msg,  
                            NMX_DECOMP_DATA *naqsbuf );
int unpack_tsdata_bundle( char *pbundle, int32_t  data1, NMXPDATAVAL *out );
int unpack_tsheader_bundle( char *pbundle, NMXPDATAHDR *header );


#endif
