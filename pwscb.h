/** @file
 * @ingroup group_proxy-waveserver
 * @brief Encapsulation of the Proxy Wave Server Control Block
 */
/***********************************************************

File Name :
        pwscb.h

Original Author:
        Pete Lombard

Description:


Creation Date:
        August 19, 2004


Modification History:


Usage Notes:


**********************************************************/

#ifndef pwscb_H
#define pwscb_H


// Proxy Wave Server control block
// This stuff has to be read by a function outside ot ProxyWaveServer class,
// so that the values can be passed to the ProxyWaveServer and WaveServer
// constructors; a bit of a mess.
typedef struct {
    unsigned int port;
    int maxclients;
    AddressList alist;
    char logfile[FILENAME_MAX];
    int lfsemkey;
    char ewfile[FILENAME_MAX];
    char nanofile[FILENAME_MAX];
    char swsfile[FILENAME_MAX];
#ifdef IRIS_WS
    char ws_file[FILENAME_MAX];
    char pws_version[MAXSTR];
#endif    
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char appName[MAXSTR];
    int no_rdns;
}pwscb;


#endif
