/** @file
 * @ingroup group_proxy-waveserver
 * @brief Abstraction of the Nanometrics Data Servers with which the Proxy Wave Server is communicating
 */
/***********************************************************

File Name :
        NanoServer.h

Original Author:
        Pete Lombard

Description:
	This class represents the Nanometrics Data Servers with which the
	Proxy Wave Server is communicating

Creation Date:
        August 19, 2004


Modification History:


Usage Notes:


**********************************************************/

#ifndef _nanoserver_h
#define _nanoserver_h

#include "WServer.h"

// Nanometrics headers:
extern "C" {
#include "nmx_pws_api.h"
}

class NanoServer : public WServer
{
 private:
    char user[NMX_DS_LEN];
    char pswd[NMX_DS_LEN];

 public:
    NanoServer();
    NanoServer(const NanoServer &s);
    NanoServer(char *addr, unsigned int ipport, char *u, char *p);
    ~NanoServer();
    TCPConnClientBuff* Connect(Duration dur);
    int GetNMXMsg(NMXHDR &hdr, char*& buffer, int &buflen, int replyTimeout);
    int Disconnect(int mode);

};

#endif
