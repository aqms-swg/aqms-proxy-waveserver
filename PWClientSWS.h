/** @file
 * @ingroup group_proxy-waveserver
 * @brief Simple Wave Server (SWS) Proxy Wave Client class
 */
/***********************************************************

File Name :
        PWClientSWS.h

Original Author:
        Pete Lombard

Description:
	Simple Wave Server (SWS) Proxy Wave Client class.


Creation Date:
        August 19, 2004


Modification History:
	January 30, 2008 - Changed to use leap second enabled lib/tntime 
	classes

	May 2, 2008 - Removed mapping of requested channel to waveserver 
		channel: no longer needed.

Usage Notes:


**********************************************************/

#ifndef pw_client_sws_H
#define pw_client_sws_H

#include "TimeWindow.h"
#include "Counter.h"
#include "PWClientBase.h"

#define PC_SWSDBSTRING "SWS.V1"

// Heirarchy of waveservers
// first, a list of servers typically serving mutually exclusive SCNLs
typedef std::list< WServer* > WServerList;

// each list of servers servers about the same SNCLs; all have same priority
typedef std::list< WServerList > WServerSet;

// Map of priority to server sets
typedef std::map<int, WServerSet, std::less<int> > WSSPrioList;

// Map of server group string (from DB) to server groups
typedef std::map<std::string, WSSPrioList, 
		 std::less<std::string> > WServerGroupMap;

class PWClientSWS : public PWClient
{
 private:
    WServerGroupMap servers;
    Counter reqid;
    std::string trim_flag;

    int _getDataFromServer(WServer *ws, Channel &chan, TimeWindow &win,
			   DataSegmentList &dl);
  
    int _swstodl(char *buffer, int getlen, DataSegmentList &dl,
		 Channel &chan, char *md5);

    double _dlCompleteness(TimeWindow win, DataSegmentList &dl, 
			     double samplerate);

 public:
    PWClientSWS(char *config);
    ~PWClientSWS();
    int LoadConfig();
    int GetData(std::string WSGroup, Channel &reqchan, 
		TimeWindow win, DataSegmentList &dl);
  
};


#endif
