# pws {#man-proxy-waveserver}

## PAGE LAST UPDATED ON

2020-08-05

## NAME

pws - Translates between the AQMS-specific protocol and several other protocols, including the Earthworm protocol

## VERSION

## DESCRIPTION

A robust, multiprocess, intermediary Proxy Wave Server (pws) translates between 
the AQMS-specific protocol and several other protocols, including the Earthworm
protocol and/or the FDSN Dataselect Web Service protocol allowing the use of 
these more commonly used wave servers.

### OPTIONS

Usage:

`pws <configfile>`

`pws` has one command line parameter, the name of its configuration file.

### CONFIGURATION FILE

#### Main Configuration File

This file provides the configuration parameters for this program.  By convention, this file is named \fIpws.cfg\fR.

| Parameter       | Description  |
|-----------------|--------------|
| `AppName`       | Provides a name for this application.  Example: `PWS-NEW` |
| `ServerPort`    | Specifies the port to use for this server.  Example: `9321` |
| `MaxClients`    | Maximum number of client.  Example: `5` |
| `ValidAddress`  | all |
| `LogFile`       | Name of the file to write log messages to.  Example `./pws.log` |
| `LogfileSemKey` | Logfile semaphore |
| `DBService`     | Database service name |
| `DBUser`        | Database user name |
| `DBPasswd`      | Database password |
| `EWFile`        | Specifies the name of a file to include for Earthworm configuration parameters.  Convention is to use `./pws_ew.cfg`.  See below for the EWFile parameters. |
| `NanoFile`      | Specifies the name of a file to include for Nanometrics configuration parameters.  Convention is to use `./pws_nano.cfg`.  See below for the NanoFile parameters. |
| `SWSFile`       | Specifies the name of a file to include for SWS configuration parameters.  Convention is to use `./pws_sws.cfg`.  See below for the SWSFile parameters. |
| `WSFile`        | Specifies the name of a file to include for IRIS WS configuration parameters.  Convention is to use `./pws_ws.cfg`.  See below for the WSFile parameters. |
| `Logfile`       | Specifies the name of a file to include containing logging configuration parameters.  Convention is to use `./pws.log`.  See below for the LogFile parameters. |


#### Earthworm Configuration File

| Parameter         | Description  |
|-------------------|--------------|
| `ConnectTimeout`  | Example: `10` |
| `MSeedRecSize`    | Example: `4096` |
| `MSeedCompMode`   | Example: `STEIM2` |
| `WSGroup`         | Example: `NCSN` |
| `SetPriority`     | Example: `1` (Can be repeated.) |
| `Server`          | Example: `wsv1.wr.usgs.gov:16020`  (Can be repeated.) |



#### Nanometric Configuration File

| Parameter         | Description  |
|-------------------|--------------|
| `User`            | Username |
| `Password`        | Password |
| `ConnectTimeout`  | Example: `10` |
| `MSeedRecSize`    | Example: `4096` |
| `MSeedCompMode`   | Example: `STEIM2` |
| `WSGroup`         | Example: `NCSN` |
| `SetPriority`     | Example: `2` |
| `Server`          | Example: `nano2.wr.usgs.gov:28002` |


#### SWS Configuration File

| Parameter       | Description  |
|-----------------|--------------|
| `TrimFlag`      | Example: `RECORD` |
| `ConnectTimeout`| Example: `10` |
| `WSGroup`       | Example: `BSL` |
| `SetPriority`   | Example: `1` |
| `Server `       | Example: `athos.geo.berkeley.edu:4999` (Can be repeated) |


#### WSFile Configuration File

| Parameter       | Description  |
|-----------------|--------------|
| `ConnectTimeout`| Example: `10` |
| `WSGroup`       | Example: `IRIS` |
| `SetPriority`   | Example: `1` |
| `URLBase`       | Example: `http://www.iris.edu/ws/dataselect` |


### ENVIRONMENT

### DEPENDENCIES

## BUG REPORTING

Bug reports can be filed on gitlab.com at https://gitlab.com/aqms-swg/aqms-proxy-waveserver/issues

