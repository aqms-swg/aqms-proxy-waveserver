# How to Compile and Install the AQMS Proxy Wave Server {#how-to-compile-install-proxy-waveserver}

## Prerequisites

This project requires that the environment variable $DEVROOT be set.

Within the $DEVROOT directory, ensure that the `/shared/makefiles/Make.includes` directory exists.

`pws` depends on aqms-libs. Ensure that these are compiled and available.

## Build Instructions

### Compilation

Building the Proxy Waveserver should be a fairly straight-forward `make` process.  It may have some dependencies on libraries outside of this project directory.

1.  Check out the code.
```
git clone https://gitlab.com/aqms-swg/aqms-proxy-waveserver.git
```

2.  Clean up old object files and executables.
```
make clean
```

3.  Build the project.
```
make all
```

### Installation

Installation instructions to be written.

