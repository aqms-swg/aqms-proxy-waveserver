/** @file
 * @ingroup group_proxy-waveserver
 * @brief IRIS Proxy Wave Client class
 */
/***********************************************************

File Name :
        PWClientIRISws.h

Original Author:
        Pete Lombard

Description:
	IRIS Web Services client


Creation Date:
        May 02, 2011


Modification History:

Usage Notes:


**********************************************************/

#ifndef pw_client_iris_ws_H
#define pw_client_iris_ws_H

#include <string>
#include <curl/curl.h>
#include "TimeWindow.h"
#include "PWClientBase.h"

#define PC_WSDBSTRING "WS.V1"

// Heirarchy of servers
// first, a list of servers (URLs) typically serving mutually exclusive SCNLs
typedef std::list< std::string > IWSServerList;

// each list of servers servers about the same SNCLs; all have same priority
typedef std::list< IWSServerList > IWSServerSet;

// Map of priority to server sets
typedef std::map<int, IWSServerSet, std::less<int> > IWSSPrioList;

// Map of server group string (from DB) to server groups
typedef std::map<std::string, IWSSPrioList, 
		 std::less<std::string> > IWSServerGroupMap;

// callback function provided to libcurl:
extern "C" size_t collectData( void *ptr, size_t size, size_t nmemb, 
			       void *userdata);

typedef struct 
{
    char *buffer;
    DataSegmentList currentData;
    DataSegmentList lastData;
    size_t buflen;
    size_t blocksize;
    size_t endData;
    bool needBlockSize;
    double samprate;
    CURL* handle;
} DataStruct;

class PWClientIRISws : public PWClient
{
 private:
    IWSServerGroupMap servers;
    DataStruct data;
    std::string pws_version;

    int _getDataFromServer(std::string &url, Channel &chan, TimeWindow &win);
  
    double _dlCompleteness(TimeWindow win, double samplerate);

 public:
    PWClientIRISws(char *config);
    ~PWClientIRISws();
    int LoadConfig();
    int GetData(std::string WSGroup, Channel &reqchan, 
		TimeWindow win, DataSegmentList &dl);
    int SetPWSVersion(char *version);
};


#endif
