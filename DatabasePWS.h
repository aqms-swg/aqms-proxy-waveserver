/** @file
 * @ingroup group_proxy-waveserver
 * @brief Database routines for the Proxy Waveserver
 */
/***********************************************************

File Name :
        DatabasePWS.h

Original Author:
        Pete Lombard

Description:


Creation Date:
        14 September, 2004

Modification History:
	May 2, 2008 - Removed mapping of requested channel to waveserver 
		channel: no longer needed.


Usage Notes:


**********************************************************/

#ifndef database_pws_H
#define database_pws_H

#include "Channel.h"
#include "Database.h"


class DatabasePWS : public Database
{
 private:

    int _lookupSampleRate(Channel &chan, double &rate, TimeStamp &time);

 protected:

 public:
    DatabasePWS();
    DatabasePWS(const char *dbs, const char *dbu, const char *dbp);
    ~DatabasePWS();

    int GetChannelString(Channel &chan, const char *appName,
			 std::string &wsType,std::string &wsGroup, 
			 TimeStamp &reqtime);
    int GetSampleRate(Channel &chan );
};



#endif
