/** @file
 * @ingroup group_proxy-waveserver
 * @brief WaveServer class
 */
/***********************************************************

File Name :
        WaveServer.h

Original Author:
        Patrick Small

Description:


Creation Date:
        16 February 1999


Modification History:
	January 30, 2008 - Changed to use leap second enabled lib/tntime 
	classes


Usage Notes:


**********************************************************/

#ifndef wave_server_H
#define wave_server_H

// Various include files
#include <map>
#include <list>
#include "TimeStamp.h"
#include "Duration.h"
#include "TimeWindow.h"
#include "Client.h"
#include "Logfile.h"
#include "TCPConn.h"
#include "TCPConnServer.h"
#include "TCPMessage.h"
#include "DataSegment.h"
#include "Channel.h"


// Type definition for a Worker-Client map
typedef std::map<int, Client, std::less<int>, std::allocator<std::pair<const int, Client> > > WorkerClientMap;


// Type definition for an Address list
typedef std::list<Client, std::allocator<Client> > AddressList;


// Type definition for a DataSegment list
typedef std::list<DataSegment, std::allocator<DataSegment> > DataSegmentList;


// Type definition for a TimeWindow list
typedef std::list<TimeWindow, std::allocator<TimeWindow> > TimeWindowList;


// Type definition for a Channel list
typedef std::list<Channel, std::allocator<Channel> > ChannelList;


class WaveServer
{
 private:
    int valid;
    WorkerClientMap workers;
    TCPConnServer conn;
    int maxclients;
    int allvalid;
    bool no_rdns;
    AddressList validaddr;

    static Client client;

    int _CreateWorker(TCPConn &clconn, Client &cl);
    int _CheckWorkers();
    int _VerifyClient(Client claddr);
    int _ProcessGetData(TCPConn &clconn, TCPMessage &msg);
    int _ProcessGetTimes(TCPConn &clconn, TCPMessage &msg);
    int _ProcessGetRate(TCPConn &clconn, TCPMessage &msg);
    int _ProcessGetChannels(TCPConn &clconn, TCPMessage &msg);
    int _SendErrorMessage(TCPConn &clconn, int respid, int errval);

 protected:
    Logfile lf;
    int lfsem;
    int serverpid;

    virtual int _GetData(Channel &chan, TimeWindow win, 
			 DataSegmentList &dl) = 0;
    virtual int _GetTimes(Channel chan, TimeWindowList &tl) = 0;
    virtual int _GetSampleRate(Channel &chan) = 0;
    virtual int _GetChannels(ChannelList &cl) = 0;
    int _StatusReport(const char *str);
    int _Cleanup();

 public:    
    WaveServer(unsigned int portnum, char *logfilename, int semkey);
    ~WaveServer();

    static Client* getClient();
    int SetValidAddress(char *host);
    void TurnOffRDNS();
    int SetMaxClients(unsigned int maxnum);
    int MainLoop();

    friend int operator!(const WaveServer &ws);
};


#endif

